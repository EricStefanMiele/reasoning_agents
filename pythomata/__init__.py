# -*- coding: utf-8 -*-

"""Top-level package for Pythomata."""

__author__ = """Marco Favorito"""
__email__ = 'marco.favorito@gmail.com'
__version__ = '0.1.5.post1'
