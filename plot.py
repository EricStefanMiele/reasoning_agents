
import matplotlib
matplotlib.use('PS')
import matplotlib.pyplot as plt
import csv
import numpy as np


def main():

   
    path2csv="./my_experiment/train.csv"

    episode = []
    step = []
    total_reward=[]
    explored_states=[]
    goal_percentage=[]


    with open(path2csv,'r') as csvfile:
        plots = csv.reader(csvfile, delimiter=';')
        i=-1
        for row in plots:
            i+=1 
            if i==0: continue
            episode.append(int(row[0]))
            step.append(int(row[1]))
            total_reward.append(float(row[2]))
            explored_states.append(int(row[3]))
            val = row[4]
            val = 1.0 if val == 'True' else 0.0
            goal_percentage.append(val)

    new_total_reward = []
    new_goal_percentage=[]

    for ii in range(len(episode)):
        tr = np.mean(sum(total_reward[:ii]))
        new_total_reward.append(tr)

        gg = np.mean(goal_percentage[max(0, ii-100):ii+1])
        new_goal_percentage.append(gg)

    total_reward = new_total_reward
    goal_percentage = new_goal_percentage
    
    pics="plots/"
    
    plt.plot(episode,step, label='Steps')
    plt.xlabel('Episodes')
    plt.ylabel('Steps')
    plt.title('Steps')
    plt.legend()
    plt.savefig(pics+"steps.ps")
    plt.clf()
    
    
    plt.plot(episode,total_reward, label='Total Reward')
    plt.xlabel('Episodes')
    plt.ylabel('Total Reward')
    plt.title('Total Reward')
    plt.legend()
    plt.savefig(pics+"total_reward.ps")
    plt.clf()
    

    plt.plot(episode,explored_states, label='Explored States')
    plt.xlabel('Episodes')
    plt.ylabel('Explored States')
    plt.title('Explored States')
    plt.legend()
    plt.savefig(pics+"explored_states.ps")
    plt.clf()
    

    plt.plot(episode,goal_percentage, label='Goal Percentage')
    plt.xlabel('Episodes')
    plt.ylabel('Goal Percentage')
    plt.title('Goal Percentage')
    plt.legend()
    plt.savefig(pics+"goal_percentage.ps")
    plt.clf()
    




   

if __name__ == "__main__":
    main()